//
//  ViewController.swift
//  ios73ls4
//
//  Created by WA on 8/29/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        printSeparatorWithTitle()
        stringExamples()
        printSeparatorWithTitle("Array example")
        arrayExample()
        printSeparatorWithTitle("Class Examples", 3)
        classesExample()
        printSeparatorWithTitle("Class vs Struct")
        classesVsStructures()
        printSeparatorWithTitle("Guard")
        boostHero(hero: nil)
        boostHero(hero: Hero(health: 100, damage: 100))
    }

    func stringExamples() {
        let name = "Student name is sValeras, is valera good boy? Valera is 12 years old"
        let messageTemplate = "Student name is"
        let newName = name.replacingOccurrences(of: "Valera", with: "Nika")
        print(newName)
        print(messageTemplate + "Valera")
        let bigString = """
//
//  ViewController.swift\(newName)
//  ios73ls4
//
//  Created by WA on 8/29/19.
//  Copyright © 2019 WA. All rights reserved.
//

"""
        print(bigString)
    }

    func arrayExample() {
        let names = [ "Artem", "Nika", "Valera", "Artur"]
        let someIndex = names.firstIndex { name -> Bool in
            name.contains("Ar")
        }
        for name in names {
            makeGreeting(name: name)
        }
    }

    func makeGreeting(name: String) {
        print("Hello, \(name)")
    }

    func printSeparatorWithTitle(_ title: String = "Default Title", _ count: Int = 1) {
        for _ in 0..<count {
            print("---------------\(title)--------------")
        }
    }

    let humanOne = Human(iqLevel: 100, health: 10, damage: 10)
    func classesExample() {
//        let humanOne = Human(iqLevel: 100, health: 50, damage: 10)
        let humanTwo = Human(iqLevel: 90, health: 10, damage: 20)
        let orcOne = Orc(handsQuantity: 2, health: 50, damage: 200)
        print("Computed test")
        print(humanTwo.physicalDamage)
        print(humanTwo.physicalDamage)
        print(humanTwo.physicalDamage)
        fightTwoHeros(heroOne: humanOne, heroTwo: orcOne)
        fightTwoHeros(heroOne: nil, heroTwo: orcOne)
        orcOne.increaseDamage()
    }

    func fightTwoHeros(heroOne: Hero?, heroTwo: Hero?) {
        // НЕ НАДО так делать
//        heroOne!.health -= heroTwo!.damage
        if heroOne == nil, heroTwo == nil {
            print("No one is ready to fight")
            return
        }
        // КРУТО! делай так
        if let heroOne = heroOne, let heroTwo = heroTwo {
            heroOne.health -= heroTwo.damage
            heroTwo.health -= heroOne.damage
            // \n - перейти на новую строку
            print("Current heroOne health: \(heroOne.health) \nCurrent heroTwo health: \(heroTwo.health)")
        }
        if heroOne == nil {
            print("Hero two is a winner")
        }
        if heroTwo == nil {
            print("Hero one is a winner")
        }
    }

    func classesVsStructures() {
        humanOne.heal()
        var someStruct = Contact(name: "Artem")
        var newStruct = someStruct
        someStruct.name = "Inna"
        newStruct.name = "Valera"
        print(someStruct.name, newStruct.name)
        
        // CLASS
        let humanTwo = humanOne

        humanOne.health -= 90
        print(humanOne.health, humanTwo.health)
    }

    // GUARD EXAMPLE
    var boostCounter = 0
    func boostHero(hero: Hero?) {
        if let hero = hero {
            hero.damage += 20
        } else {
            return
        }
//        guard let hero = hero else { return }
//        hero.damage += 20
        
        boostCounter += 1
        
    }
}

