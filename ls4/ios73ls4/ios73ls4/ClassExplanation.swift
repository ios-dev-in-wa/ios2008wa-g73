//
//  ClassExplanation.swift
//  ios73ls4
//
//  Created by WA on 8/29/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Hero {
    var health: Int {
        willSet {
            if health <= 0 {
                print("You are dead")
            }
        }
    }
    var damage: Int {
        didSet {
            print("New damage is \(damage)")
        }
    }
//    {
//        set {
//            print("New value is setted")
//
//        }
//        get {
//            return physicalDamage
//        }
//    }

    var physicalDamage: Int {
        return Int.random(in: 0..<damage)
    }

    init(health: Int, damage: Int) {
        self.health = health
        self.damage = damage
    }
}

class Human: Hero {
    var iqLevel: Int

    init(iqLevel: Int, health: Int, damage: Int) {
        self.iqLevel = iqLevel
        super.init(health: health, damage: damage)
    }

    func heal() {
//        self.health += 10
        health += 10
    }
}

class Orc: Hero {
    var handsQuantity: Int
    init(handsQuantity: Int, health: Int, damage: Int) {
        self.handsQuantity = handsQuantity
        super.init(health: health, damage: damage)
    }

    func increaseDamage() {
        damage += 10
    }
}

struct Contact {
    var name: String
}
