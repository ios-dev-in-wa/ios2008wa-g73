//
//  ViewController.swift
//  test
//
//  Created by Dmitry Timchuk on 8/23/19.
//  Copyright © 2019 Footies. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        let more = "More"
//        var noMore = "MOre"
        // + - * / %
//        let intMod = 5 % 2
////        let mod = 5.0.truncatingRemainder(dividingBy: 2) //
//        print(intMod)
//        var counter = 1
//        counter = counter + 1
//        print(counter)
//        // += -= /= *= %=
//        counter += 1
//        counter %= 2
//        print(counter)
//        // < > == != => <=
//        if counter == 1 {
//            print("COUNTER == 1")
//            counter -= 1
//        }
//        if counter != 1 {
//            print("COUNTER IS NOT 1, it is: ", counter)
//        }
//        let isYoung = true
//        let isRich = false
//        if isYoung, isYoung == isRich {
//            print("Yes you are young and rich")
//        } else {
//            print("Goto univer")
//        }
        // && ||
//        print(checkIfYoungAndRich(age: 18, money: 2000))
//        print(getIntRandom())
//        print(getSquareOfRandomNumbers())
//        print(getSquareOfRandomNumbers())
//        roundedExample()
//        print(counter, counter, counter, separator: "HELLO")
        some()
//        dictionaryExample()
    }

//    func checkIfYoungAndRich() -> String {
//        return checkIfYoungAndRich(age: 10, money: 1002.0)
//    }

    func checkIfYoungAndRich(age: Int, money: Double) -> String {
        var textContainer = ""
//        if age < 25, money > 1000.0 {
//            textContainer = "Success"
//        }
        textContainer = age < 25 && money > 1000.0 ? "Success" : "Not ready"
//        if age <= 18 {
//            textContainer = "Go to school"
//        }
        textContainer = age <= 18 ? "Go to school" : "Not ready"
        if money < 1000 {
            textContainer = "Go to work"
        }
        return textContainer
    }

    func getIntRandom() -> Int {
        return Int.random(in: 0..<4)
    }

    func getSquareOfRandomNumbers() -> Int {
        let randomValue = Int.random(in: 1...20)
        return randomValue * randomValue
    }
    
    func getSquareOfDouble() -> Double {
        let randomValue = Double.random(in: 0.5...2.0)
        return randomValue * randomValue
    }

    func roundedExample() {
        let valueOne = 1.2
        let valueTwo = 2.6
        let sum = valueOne + valueTwo
        print(sum)
        print(sum.rounded())
    }

    func switchExample() {
        let value = Int.random(in: 0...4)
        switch value {
        case 0:
            print("It's NULL")
        case 1:
            print("It's One")
        default:
            break
//            print("It's default")
        }
    }

    enum HeroRace {
        case human, orc, elf
        case darkElf
    }

    func enumSwitch() {
        let race = HeroRace.human
        switch race {
        case .elf:
            print("He is Elf")
        case .human:
            print("He is Human")
        case .orc:
            print("He is ORC")
        default:
            break
        }
    }

    func some() {
        let someString = "Artem"
//        someString.lowercased()
//        someString.isEmpty
//        someString.count < 200
        let somePin = someString + "name" + " Nina"
        print(somePin)
//        print(somePin.replacingOccurrences(of: "name", with: "lastName"))
        var names = ["Artem", "Valera", "Nina"]
//        var ages = [12, 24, 53]
//        print(names)
//        names.append("Vitaliy")
//        print(names)
//        names.remove(at: 1)
//        print(names)
//        names.count
        let index = 1
        if index <= names.count - 1 {
            let first = names[index]
            print(first)
        }
        var anyNames: [Any] = ["Artem", 1, 2.0]
//        if let intVal = anyNames[1] as? Int {
//
//        }
//        if let stringVal = anyNames[1] as? String {
//
//        }
//        let anyVal = anyNames[1] as? Int
    }

    func dictionaryExample() {
        var dict = [
            "Artem": 18,
            "Valera": 19,
            "Nina": 23
        ]
        print(dict["Artem"])
        print(dict.keys)
        print(dict.values)
        dict["Nicka"] = 20
        dict.removeValue(forKey: "Artem")
        print(dict)
//        let dictArrayExample = [
//            "names": ["Artem", "Valera"],
//            "ages": ["12", "12"],
//            "gender": ["Male", "Male"]
//        ]
//        dictArrayExample
    }
}
