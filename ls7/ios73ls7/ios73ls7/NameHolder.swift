//
//  NameHolder.swift
//  ios73ls7
//
//  Created by WA on 9/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class NameHolder {

    static let shared = NameHolder()

    var name: String = "No name"
}
