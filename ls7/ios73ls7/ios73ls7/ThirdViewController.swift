//
//  ThirdViewController.swift
//  ios73ls7
//
//  Created by WA on 9/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        print("Third did load")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("Third will appear")
    }

    @IBAction func didPressButton(_ sender: UIButton) {
        NameHolder.shared.name = "No name"
    }

    @IBAction func toggleSwitch(_ sender: UISwitch) {
        if sender.isOn {
            print("I am ready")
        }
    }

    @IBAction func didSwithcSegment(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
    }

    @IBAction func didChangeSliderValue(_ sender: UISlider) {
        view.alpha = CGFloat(sender.value)
    }
}
