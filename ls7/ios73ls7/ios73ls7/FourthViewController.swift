//
//  FourthViewController.swift
//  ios73ls7
//
//  Created by WA on 9/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

protocol FourthViewControllerDelegate: class {
    func didEnterName(_ name: String)
}

class FourthViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!

    weak var delegate: FourthViewControllerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let saveBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveInfo))
        navigationItem.leftBarButtonItem = saveBarButtonItem
    }

    @objc func saveInfo() {
        delegate?.didEnterName(textField.text ?? "Did not enter anything")
        navigationController?.dismiss(animated: true, completion: nil)
//        dismiss(animated: true, completion: nil)

        // Non modal
//        navigationController?.popViewController(animated: true)
    }
}
