//
//  ViewController.swift
//  ios73ls7
//
//  Created by WA on 9/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // TO layout subviews
//        view.layoutIfNeeded()
//        present(<#T##viewControllerToPresent: UIViewController##UIViewController#>, animated: <#T##Bool#>) {
//
//        }
        print("First did load")
        tabBarController?.tabBar.tintColor = .red
        navigationController?.navigationBar.isHidden = true
        navigationItem.title = "TESST"
        navigationItem.largeTitleDisplayMode = .always
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("First will appear")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let navigationController = navigationController { navigationController.navigationBar.isHidden = !navigationController.navigationBar.isHidden
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        switch segue.identifier {
        case "segueToNameVC":
            if let navigationVC = segue.destination as? UINavigationController,
                let destinationVC = navigationVC.viewControllers.first as? FourthViewController {
                destinationVC.delegate = self
            }
        default: break
        }
    }

//    override func didReceiveMemoryWarning() {
//        super.didReceiveMemoryWarning()
//    }
}

extension FirstViewController: FourthViewControllerDelegate {
    func didEnterName(_ name: String) {
        NameHolder.shared.name = name
        print(name)
    }
}
