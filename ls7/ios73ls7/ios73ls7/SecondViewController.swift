//
//  SecondViewController.swift
//  ios73ls7
//
//  Created by WA on 9/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var label: UILabel!

//    override func awakeFromNib() {
//        super.awakeFromNib()
//
//    }

    override func viewDidLoad() {
        super.viewDidLoad()

//        INIT
//        let customView = CustomView()
//        view.addSubview(customView)
//        customView.center = view.center
        // nib
        if let customView: CustomView = .fromNib() {
            view.addSubview(customView)
            customView.center = view.center
        }
        print("SECOND did load")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        print("SECOND will appear")
        label.text = NameHolder.shared.name
    }
}
