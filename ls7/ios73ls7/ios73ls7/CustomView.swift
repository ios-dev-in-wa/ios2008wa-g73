//
//  CustomView.swift
//  ios73ls7
//
//  Created by WA on 9/10/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class CustomView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .red
    }
//    init() {
//        super.init(frame: CGRect.zero)
//        frame.size.width = 100
//        frame.size.height = 100
//        frame.origin.x = 0
//        frame.origin.y = 10
//        backgroundColor = .black
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
}
