//
//  User.swift
//  ios73ls8
//
//  Created by WA on 9/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

final class User {

    private init() { }

    static let shared = User()

    let currentUser = UserModel(name: "Valera", surname: "Babkin", phoneNum: "+38088888888", avatarImage: UIImage(named: "Valera"))
}
