//
//  UserTableViewCell.swift
//  ios73ls8
//
//  Created by WA on 9/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    var user: UserModel?

    override func awakeFromNib() {
        super.awakeFromNib()
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.height / 2
        avatarImageView.layer.borderWidth = 1
        avatarImageView.layer.borderColor = UIColor.red.cgColor
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        user = nil
    }

    func setupWith(user: UserModel) {
        self.user = user
        titleLabel.text = user.name
        detailLabel.text = user.phoneNum
        let date = Date()
        print(date.timeIntervalSince1970)
        let formater = DateFormatter()
        formater.dateFormat = "HH:mm"
        dateLabel.text = formater.string(from: date)
        avatarImageView.image = user.avatarImage
    }
}
