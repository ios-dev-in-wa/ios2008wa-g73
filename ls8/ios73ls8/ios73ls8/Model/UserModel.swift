//
//  User.swift
//  ios73ls8
//
//  Created by WA on 9/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

struct UserModel {
    let name: String
    let surname: String
    let phoneNum: String
    let avatarImage: UIImage?
}
