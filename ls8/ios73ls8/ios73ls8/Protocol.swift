//
//  Protocol.swift
//  ios73ls8
//
//  Created by WA on 9/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

protocol Beast {
    var toothQuantity: Int { get }
    var furColor: UIColor { get set }
}

protocol PoraRoky {
    func summerCome()
    func winterCome()
}

class Rabbit: Beast, PoraRoky {
    var toothQuantity: Int = 20
    
    var furColor: UIColor = .gray

    func summerCome() {
        furColor = .gray
    }

    func winterCome() {
        furColor = .white
    }
}

class Bear: Beast, PoraRoky {
    var toothQuantity: Int = 40
    
    var furColor: UIColor = .brown
    
    func summerCome() {
        huntAndEat()
    }
    
    func winterCome() {
        sleepAndSuckLAPA()
    }

    func sleepAndSuckLAPA() { }

    func huntAndEat() { }
}

class Nature {

    let rabbit = Rabbit()
    let bear = Bear()

    func live() {
        //...
        itsTimeToChangePoraRoky()
        //....
    }

    func itsTimeToChangePoraRoky() {
        rabbit.summerCome()
        bear.summerCome()
    }
}
