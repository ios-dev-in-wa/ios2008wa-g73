//
//  ViewController.swift
//  ios73ls8
//
//  Created by WA on 9/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    var user: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("viewDidLoad")
        nameLabel.text = user?.name
        surnameLabel.text = user?.surname
        phoneLabel.text = user?.phoneNum
        avatarImageView.image = user?.avatarImage
    }

    func setupWith(user: UserModel) {
        self.user = user
    }
}

