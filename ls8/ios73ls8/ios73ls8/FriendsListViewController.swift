//
//  FriendsListViewController.swift
//  ios73ls8
//
//  Created by WA on 9/12/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class FriendsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!

    let friendsList = [
        UserModel(name: "Artem", surname: "Veykyy", phoneNum: "+333333333333333333333333333333333333333333333333333333333333333", avatarImage: UIImage(named: "placeholder")),
        UserModel(name: "Inna", surname: "Nomik", phoneNum: "+33333353", avatarImage: UIImage(named: "placeholder")),
        UserModel(name: "Valera", surname: "Burda", phoneNum: "+333444433", avatarImage: UIImage(named: "placeholder"))
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "userCell")
        tableView.tableFooterView = UIView()

        let header: TableViewHeaderView? = .fromNib()
        tableView.tableHeaderView = header
//        tableView.register(UserTableViewCell.self, forCellReuseIdentifier: "userCell")
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let headerView = tableView.tableHeaderView {
            
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            //Comparison necessary to avoid infinite loop
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                tableView.tableHeaderView = headerView
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath)
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? UserTableViewCell else { return UITableViewCell() }
//        let userModel = friendsList[indexPath.row]
//        cell.textLabel?.text = userModel.name
//        cell.detailTextLabel?.text = userModel.phoneNum
        cell.setupWith(user: friendsList[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueToTheProfile", sender: friendsList[indexPath.row])
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToTheProfile",
            let destinationVC = segue.destination as? ViewController,
            let user = sender as? UserModel {
            destinationVC.setupWith(user: user)
        }
    }
}


extension UIView {
    class func fromNib<T: UIView>() -> T? {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as? T
    }
}
