//
//  NotificationName+Extension.swift
//  ios73ls6
//
//  Created by WA on 9/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let customNotification: Notification.Name = Notification.Name(rawValue: "TESTNOTIFICATION")
}
