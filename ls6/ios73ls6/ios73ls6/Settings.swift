//
//  Settings.swift
//  ios73ls6
//
//  Created by WA on 9/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct Settings {
    static var launchCount: Int {
        get { return UserDefaults.standard.integer(forKey: "launchCount") }
        set { UserDefaults.standard.set(newValue, forKey: "launchCount") }
    }
}
