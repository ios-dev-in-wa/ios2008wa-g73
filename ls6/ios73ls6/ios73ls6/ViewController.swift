//
//  ViewController.swift
//  ios73ls6
//
//  Created by WA on 9/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class Animal {
    var isHeadPersist: Bool = true
}

protocol Hishnik {
    var isKogtiPersist: Bool { get }
    func eat(animal: Animal)
}

class Wolf: Animal, Hishnik {
    var isKogtiPersist: Bool = true
    
    func eat(animal: Animal) {
        print("Wold eat animal")
    }
}

class ViewController: UIViewController, UITextViewDelegate, ButtonVCDelegate {

    @IBOutlet weak var label: UILabel!
    let someTest = "SOME"
    let button = UIButton()
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var textView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        NotificationCenter.default.addObserver(self, selector: #selector(orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(didGetCustomNotification), name: .customNotification, object: nil)
//        savingToThePlist()
//        readFromDisk()
//        print(Settings.launchCount)
//        colorActions()
        textView.delegate = self
        let attributed = "attributed"
        let text = "I will be attributed"
        let attributedText = NSMutableAttributedString(string: text)
       let range = (text as NSString).range(of: attributed)
        attributedText.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        textView.attributedText = attributedText
        label.attributedText = attributedText

        view.backgroundColor = UIColor(named: "purpleTest")

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print("Will layout ", view.frame)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print("DID layout", view.frame)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "GoToButtonVC" {
            if let buttonVC = segue.destination as? ButtonViewController {
                buttonVC.delegate = self
            }
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    @objc func orientationDidChange() {
        print("ORIENTATION DID CHANGE")
    }

    @objc func didGetCustomNotification() {
        print("didGetCustomNotification")
    }

    @IBAction func didPressButton(_ sender: UIButton) {
        NotificationCenter.default.post(name: .customNotification, object: nil)
    }

    func savingToThePlist() {
        let stringArray = ["Indigo", "Castle", "2"]
        let nsStrngArray = stringArray as NSArray
        nsStrngArray.write(toFile: "/Users/wa/Desktop/someArray.plist" , atomically: false)
    }

    func readFromDisk() {
        let nsArray = NSArray(contentsOfFile: "/Users/wa/Desktop/someArray.plist")
        print(nsArray as? [String])
    }

    func colorActions() {
        let color = hexStringToUIColor(hex: "CCFFE5")
        view.backgroundColor = color
    }

    @IBAction func textFieldEdnEditing(_ sender: UITextField) {
        print(sender.text)
    }

    func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        print(textView.text)
    }

    func didPressLogin(login: String?) {
        let someOptionalString: String? = nil//"Test"
        let someOtherNilString: String? = nil
        print(someOptionalString ?? someOtherNilString ?? "TestTwo")
        print(login ?? "")
    }

//    func didPressButton() {
//        print("Button is pressed, im first VC")
//    }
}

