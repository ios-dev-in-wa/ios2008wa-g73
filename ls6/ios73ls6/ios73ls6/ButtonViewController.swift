//
//  ButtonViewController.swift
//  ios73ls6
//
//  Created by WA on 9/5/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

protocol ButtonVCDelegate: class {
    func didPressLogin(login: String?)
}

class ButtonViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!

    @IBOutlet weak var imageView: UIImageView!
    weak var delegate: ButtonVCDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func didPressButton(_ sender: UIButton) {
        imageView.image = UIImage(named: "screenShot")
        delegate?.didPressLogin(login: textField.text)
    }
}
