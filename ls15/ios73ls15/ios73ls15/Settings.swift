//
//  Settings.swift
//  ios73ls15
//
//  Created by WA on 10/8/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Settings {

    static let shared = Settings()

    private let defaults = UserDefaults.standard

    var nameValue: String? {
        get {
            return defaults.string(forKey: "name_preference")
        }
        set {
            defaults.set(newValue, forKey: "name_preference")
        }
    }
}
