//
//  String+localized.swift
//  ios73ls15
//
//  Created by WA on 10/8/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
