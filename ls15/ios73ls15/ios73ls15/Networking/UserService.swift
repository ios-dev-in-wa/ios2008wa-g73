//
//  UserService.swift
//  ios73ls15
//
//  Created by WA on 10/8/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct UserService {

    private let session = URLSession.shared

    func getUsers(completionHandler: @escaping (([User]) -> Void)) {
        guard let url = URL(string: "https://randomuser.me/api/?results=10") else { return }

        let dataTask = session.dataTask(with: url) { (data, response, error) in
            if let erorr = error {
                print(erorr.localizedDescription)
                return
            }
            guard let data = data else { return }
            do {
                let userResponse = try JSONDecoder().decode(UserResponse.self, from: data)
                completionHandler(userResponse.results)
            } catch {
                print(error.localizedDescription)
            }
        }

        dataTask.resume()
    }
}
