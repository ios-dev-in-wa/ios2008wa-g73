//
//  ViewController.swift
//  ios73ls15
//
//  Created by WA on 10/8/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var autoLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        FileStorage.shared.readData { profiles in
            print(profiles)
        }
        FileStorage.shared.writeData(profiles: [
            Profile(name: "Nina", surname: "Kalina"),
            Profile(name: "Artem", surname: "Boliva"),
            Profile(name: "Harton", surname: "Bell"),
            Profile(name: "Geoge", surname: "Kumbell"),
        ])
        print(Locale.current.regionCode)
        print(Locale.current.languageCode)
//        let value = NSLocalizedString("autoKey", comment: "")
        autoLabel.text = "autoKey".localized
        let searchableText = "whitespace@gmail.com"
        let range = NSRange(location: 0, length: searchableText.count)
        let regularExpression = try? NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}")
        print("is email valid", regularExpression?.firstMatch(in: searchableText, options: [], range: range) != nil)

        let emailFormat = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPredicate = NSPredicate(format:"SELF MATCHES %@", emailFormat)
        print(emailPredicate.evaluate(with: searchableText))

//        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
//            self?.openAppSettings()
//        }
        UserService().getUsers { users in
            print(users)
        }
    }

    func openAppSettings() {
        UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!, options: [:], completionHandler: nil)
    }
}
