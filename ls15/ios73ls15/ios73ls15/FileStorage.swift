//
//  FileStorage.swift
//  ios73ls15
//
//  Created by WA on 10/8/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct Profile: Codable {
    let name: String
    let surname: String
}

final class FileStorage {

    static let shared = FileStorage()

    private init() { }

    private let dataFileName = "MyData"
    private let fileManager = FileManager.default
    private let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first

    private var dataFileUrl: URL? {
        return documentsDirectory?.appendingPathComponent(dataFileName)
    }

    func writeData(profiles: [Profile]) {
        // 1 optinal unwrap documents path
//        guard let documetsDirectory = documentsDirectory else { return }
        // 2 append path with data name
//        let dataFilePath = documetsDirectory.appendingPathComponent(dataFileName)
        // new realization
        guard let dataFilePath = dataFileUrl else { return }
        // 3 unsure that file is not exist yet
        if fileManager.fileExists(atPath: dataFilePath.absoluteString) {
            do {
                // 4 remove file if it is exist
                try fileManager.removeItem(at: dataFilePath)
            } catch {
                print(error.localizedDescription)
            }
        }
        do {
            // 5 Encode Profiles as Data
            let data = try JSONEncoder().encode(profiles)
            // 6 Write data to path
            try data.write(to: dataFilePath)
        } catch {
            print(error.localizedDescription)
        }
    }

    func readData(completionHandler: (([Profile]) -> Void)){
        guard let dataFilePath = dataFileUrl else { return }

        do {
            let data = try Data(contentsOf: dataFilePath)
            let decodedProfiles = try JSONDecoder().decode([Profile].self, from: data)
            completionHandler(decodedProfiles)
        } catch {
            print(error.localizedDescription)
        }
    }
}
