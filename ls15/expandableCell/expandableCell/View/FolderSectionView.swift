//
//  FolderSectionView.swift
//  expandableCell
//
//  Created by WA on 10/15/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

protocol NameFolderSectionViewDelegate: class {
    func didPressButton(section: Int)
}

class FolderSectionView: UIView {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    private var section: Int = 0

    weak var delegate: NameFolderSectionViewDelegate?
    
    func setupView(with model: MenuItem, section: Int) {
        self.section = section
        nameLabel.text = model.name
    }

    @IBAction func buttonDidPressed(sender: UIButton) {
        delegate?.didPressButton(section: section)
    }
}
