//
//  ViewController.swift
//  expandableCell
//
//  Created by WA on 10/15/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

// MODEL
struct MenuItem {
    let name: String
    let isExpandable: Bool
    let value: Int?
    let items: [MenuItem]
}
// END MODEL

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    let dataForMenu: [MenuItem] = [
        MenuItem(name: "inbox", isExpandable: false, value: 1, items: [
            MenuItem(name: "Sageman extention", isExpandable: false, value: 2, items: [])
        ]),
        MenuItem(name: "Assignet to me", isExpandable: false, value: 1, items: [
        MenuItem(name: "Sageman extention", isExpandable: false, value: 2, items: [])
        ]),
        MenuItem(name: "Work", isExpandable: true, value: 1, items: [
            MenuItem(name: "Sageman extention", isExpandable: false, value: 2, items: []),
            MenuItem(name: "Sageman extention", isExpandable: false, value: 2, items: []),
            MenuItem(name: "Sageman extention", isExpandable: false, value: 2, items: [])
        ])
    ]
    
    var isSectionExpanded: [Bool] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        fetchData()
    }

    func fetchData() {
        // get data from server ....
        // AFTER we fetch DATA
        isSectionExpanded = Array(repeating: false, count: dataForMenu.count)
    }
//
//    @objc func didPressExpandButton(button: UIButton) {
//        let value = isSectionExpanded[button.tag]
//        isSectionExpanded.remove(at: button.tag)
//        isSectionExpanded.insert(!value, at: button.tag)
//        tableView.reloadSections([button.tag], with: .automatic)
//    }

}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return dataForMenu.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 36
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let folderView: FolderSectionView = .fromNib()
        folderView.setupView(with: dataForMenu[section], section: section)
        folderView.delegate = self
        return folderView
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSectionExpanded[section] ? dataForMenu[section].items.count : 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "folderViewCell", for: indexPath) as? FolderHeaderViewCell else { return UITableViewCell() }
//        cell.folderNameLabel.text = dataForMenu[indexPath.section].items[indexPath.row].name//objects[indexPath.row].object(forKey: "folderName") as? String
        let cell = UITableViewCell()
        cell.textLabel?.text = dataForMenu[indexPath.section].items[indexPath.row].name
        return cell
    }
    
}

extension ViewController: NameFolderSectionViewDelegate {
    func didPressButton(section: Int) {
        let value = isSectionExpanded[section]
        isSectionExpanded.remove(at: section)
        isSectionExpanded.insert(!value, at: section)
        tableView.reloadSections([section], with: .automatic)
    }
}


extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

