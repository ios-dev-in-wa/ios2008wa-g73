//
//  ViewController.swift
//  anyfood
//
//  Created by Dmitry Timchuk on 9/16/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var panView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var avatar: UIImageView!

    let speedList = [10, 15, 100, 20, 50, 123, 123, 424]
    var initialCenterPoint: CGPoint = .zero
    
    var user: UserModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = user?.name
        surnameLabel.text = user?.surname
        phoneLabel.text = user?.phone
        avatar.image = user?.avatar

//        setDataCount(10, range: 40)
    }
    
    func setupWith(user: UserModel) {
        self.user = user
    }

//    func setDataCount(_ count: Int, range: UInt32) {
//        let entries = (0..<count).map { (i) -> PieChartDataEntry in
//            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
//            return PieChartDataEntry(value: Double(arc4random_uniform(range) + range / 5),
//                                     label: "Test",
//                                     icon: nil)
//        }
//
//        let set = PieChartDataSet(entries: entries, label: "Election Results")
//        set.drawIconsEnabled = false
//        set.sliceSpace = 2
//
//
//        set.colors = ChartColorTemplates.vordiplom()
//            + ChartColorTemplates.joyful()
//            + ChartColorTemplates.colorful()
//            + ChartColorTemplates.liberty()
//            + ChartColorTemplates.pastel()
//            + [UIColor(red: 51/255, green: 181/255, blue: 229/255, alpha: 1)]
//
//        let data = PieChartData(dataSet: set)
//
//        let pFormatter = NumberFormatter()
//        pFormatter.numberStyle = .percent
//        pFormatter.maximumFractionDigits = 1
//        pFormatter.multiplier = 1
//        pFormatter.percentSymbol = " %"
//        data.setValueFormatter(DefaultValueFormatter(formatter: pFormatter))
//
//        data.setValueFont(.systemFont(ofSize: 11, weight: .light))
//        data.setValueTextColor(.white)
//
//        pieChartView.data = data
//        pieChartView.highlightValues(nil)
//    }

    @IBAction func panAction(_ sender: UIPanGestureRecognizer) {
        switch sender.state {
        case .began:
            self.panView.backgroundColor = .purple
            initialCenterPoint = sender.location(in: view)
            print(sender.location(in: view))
        case .changed:
            panView.center = sender.location(in: view)
        case .ended:
            UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: [.curveEaseInOut, .repeat], animations: {
                 self.panView.center = self.initialCenterPoint
            }, completion: { _ in
                self.panView.backgroundColor = .black
                print("animation ended")
            })
        default: break
        }
    }
}

