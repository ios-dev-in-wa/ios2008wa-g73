//
//  Protocol.swift
//  anyfood
//
//  Created by Dmitry Timchuk on 9/16/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

protocol Beast {
    var toothQuontity: Int { get }
    var furColor: UIColor { get set }
    
}

protocol Times {
    func summerCome()
    func winterCome()
}

class Rabbit: Beast, Times {
    var toothQuontity: Int = 20
    
    var furColor: UIColor = .gray
    
    func summerCome() {
        furColor = .gray
    }
    
    func winterCome() {
        furColor = .white
    }
}

class Bear: Beast, Times {
    var toothQuontity: Int = 40
    
    var furColor: UIColor = .brown
    
    func summerCome() {
        huntAndEat()
    }
    
    func winterCome() {
        sleepAndSuckLapa()
    }
    
    func sleepAndSuckLapa() {
        
    }
    
    func huntAndEat() {
        
        }
}

class Nature {
    let rabbit = Rabbit()
    let bear = Bear()
    
    func live() {
        itsTimeToChangeTime()
    }
    
    func itsTimeToChangeTime() {
        rabbit.summerCome()
        bear.summerCome()
    }
}
