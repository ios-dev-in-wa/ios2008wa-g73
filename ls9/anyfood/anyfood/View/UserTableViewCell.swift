//
//  UserTableViewCell.swift
//  anyfood
//
//  Created by WA on 9/17/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    func setupWith(user: UserModel) {
        titleLabel.text = user.name
        detailLabel.text = user.phone
        dateLabel.text = "TODAY"
    }
    
}
