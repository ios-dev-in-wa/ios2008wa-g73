//
//  User.swift
//  anyfood
//
//  Created by Dmitry Timchuk on 9/16/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

final class User {
    private init() {}
    
    static let shared = User()
    
    let currentUser = UserModel(name: "Valera", surname: "Babkin", phone: "+3808888888", avatar: UIImage(named: "valera"))
    
}
