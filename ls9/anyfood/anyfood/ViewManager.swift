//
//  ViewManager.swift
//  anyfood
//
//  Created by WA on 9/17/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

final class ViewManager {

    static let shared = ViewManager()

    private init() { }

    private var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }

    func showLaunchSpinnerVC() {
        let storyboard = UIStoryboard(name: "Login", bundle: Bundle.main)
        appDelegate?.window?.rootViewController = storyboard.instantiateViewController(withIdentifier: "launchSpinnerVC")
    }

    func showMainVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        appDelegate?.window?.rootViewController = storyboard.instantiateInitialViewController()
    }
}
