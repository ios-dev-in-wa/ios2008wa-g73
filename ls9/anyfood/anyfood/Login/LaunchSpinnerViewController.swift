//
//  LaunchSpinnerViewController.swift
//  anyfood
//
//  Created by WA on 9/17/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

class LaunchSpinnerViewController: UIViewController {
    
    let button = UIButton(frame: CGRect(x: 20, y: 20, width: 100, height: 100))

    var container: Infinity?

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        container = Infinity.createGeometricLoader()
        container?.startAnimation()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        button.backgroundColor = .blue
        button.setTitle("Tap me", for: .normal)
        button.addTarget(self, action: #selector(buttonAction(_:)), for: .touchUpInside)
        view.addSubview(button)
        view.bringSubviewToFront(button)
    }

    @objc func buttonAction(_ sender: UIButton) {
        performSegue(withIdentifier: "goToMainStoryboard", sender: nil)
    }
}
