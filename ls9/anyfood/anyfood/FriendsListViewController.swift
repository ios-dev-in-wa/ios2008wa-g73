//
//  MyFriendsViewController.swift
//  anyfood
//
//  Created by Dmitry Timchuk on 9/16/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

class FriendsListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    let friendsList = [
        UserModel(name: "Artem", surname: "Vladimirovich", phone: "+3806789352", avatar: nil),
        UserModel(name: "Jhanna", surname: "Aleksandrovna", phone: "+38067123122", avatar: nil),
        UserModel(name: "Natalia", surname: "Urievna", phone: "+3806789352", avatar: nil)
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "UserTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "userCell")
        tableView.tableFooterView = UIView()
//        tableView.estimatedRowHeight = 100
//        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return friendsList.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: "segueToProfile", sender: friendsList[indexPath.row])
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? UserTableViewCell else { return UITableViewCell() }
        //let userModel = friendsList[indexPath.row]
        
        
//        cell.textLabel?.text = userModel.name
//        cell.detailTextLabel?.text = userModel.phone
        
        cell.setupWith(user: friendsList[indexPath.row])
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueToProfile",
            let destinationVC = segue.destination as? ViewController,
            let user = sender as? UserModel {
            
            destinationVC.setupWith(user: user)
        }
    }

}
