//
//  User.swift
//  anyfood
//
//  Created by Dmitry Timchuk on 9/16/19.
//  Copyright © 2019 personal. All rights reserved.
//

import UIKit

struct UserModel {
    let name: String
    let surname: String
    let phone: String
    let avatar: UIImage?
}
