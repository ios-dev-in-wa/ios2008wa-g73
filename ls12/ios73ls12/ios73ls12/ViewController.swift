//
//  ViewController.swift
//  ios73ls12
//
//  Created by WA on 9/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    @IBOutlet weak private var tableView: UITableView!
    private let searcBar = UISearchBar()

    private(set) var heroes = [Hero]() {
        didSet {
            currentHeroes = heroes
        }
    }
    private var currentHeroes = [Hero]() {
        didSet {
            tableView.reloadData()
        }
    }
    private let service = PeopleService()
    
    @objc class Person: NSObject {
        @objc dynamic var name = "Taylor Swift"
    }

    let taylor = Person()
    var observer: NSKeyValueObservation?

    override func viewDidLoad() {
        super.viewDidLoad()
        searcBar.delegate = self
        searcBar.frame.size = CGSize(width: tableView.bounds.width, height: 50)
        tableView.tableHeaderView = searcBar
        tableView.delegate = self
        tableView.dataSource = self
        service.getPeoples { [weak self] heroes in
            self?.heroes = heroes
        }
//        observer = taylor.observe(\Person.name, options: .new) { person, change in
//            print("I'm now called \(person.name)")
//        }
        observer = view.observe(\UIView.backgroundColor, options: .new) { (view, change) in
            view.backgroundColor = .red
        }
        taylor.name = "Valera"
//        taylor.observe(<#T##keyPath: KeyPath<ViewController.Person, Value>##KeyPath<ViewController.Person, Value>#>, changeHandler: <#T##(ViewController.Person, NSKeyValueObservedChange<Value>) -> Void#>)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.backgroundColor = .black
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentHeroes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "peopleCell", for: indexPath)
        cell.textLabel?.text = currentHeroes[indexPath.row].name
        cell.detailTextLabel?.text = currentHeroes[indexPath.row].homeworld

        return cell
    }
}

extension ViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard !searchText.isEmpty else {
            currentHeroes = heroes
            return
        }
        service.getHeroesByWord(searchText) { [weak self] heroes in
            self?.currentHeroes = heroes
        }
    }
}

extension UIImageView {
    func downloaded(from url: URL, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        contentMode = mode
        let activityIndicator = UIActivityIndicatorView()
        addSubview(activityIndicator)

        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
            ])
        activityIndicator.style = .gray
        activityIndicator.startAnimating()
        URLSession.shared.dataTask(with: url) { data, response, error in
            DispatchQueue.main.async {
                activityIndicator.stopAnimating()
            }
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
            }.resume()
    }

    func downloaded(from link: String, contentMode mode: UIView.ContentMode = .scaleAspectFit) {  // for swift 4.2 syntax just use ===> mode: UIView.ContentMode
        guard let url = URL(string: link) else { return }
        downloaded(from: url, contentMode: mode)
    }
}
