//
//  DetailViewController.swift
//  ios73ls12
//
//  Created by WA on 9/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var iamgeView: UIImageView!
    var observer: NSKeyValueObservation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        iamgeView.downloaded(from: "http://getwallpapers.com/wallpaper/full/7/d/b/13657.jpg")
        observer = view.observe(\UIView.backgroundColor, options: .new) { (view, change) in
            guard change.newValue != .red else { return }
            DispatchQueue.main.async { [weak self] in
                self?.view.backgroundColor = .red
            }
        }
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.backgroundColor = .black
    }
}
