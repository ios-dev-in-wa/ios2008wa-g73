//
//  HeroesResponse.swift
//  ios73ls12
//
//  Created by WA on 9/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct HeroesResponse: Codable {
    let results: [Hero]
}
