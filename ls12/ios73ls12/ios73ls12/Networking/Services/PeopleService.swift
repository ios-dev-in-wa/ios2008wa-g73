//
//  SwapiService.swift
//  ios73ls12
//
//  Created by WA on 9/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct PeopleService {
    private let api = "https://swapi.co/api/"
    private let session = URLSession.shared

    func getPeoples(completionHandler: @escaping ([Hero]) -> Void) {
        guard let url = URL(string: api + "people") else {
            return
        }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15.0)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard (response as? HTTPURLResponse)?.statusCode == 200,
            let data = data else { return }
            // BEFORE
//            guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else { return }
//            print(jsonData["count"])
            
            // NOW
            guard let heroesResponse = try? JSONDecoder().decode(HeroesResponse.self, from: data) else { return }
            DispatchQueue.main.async {
                completionHandler(heroesResponse.results)
            }
//            print(String(data: data, encoding: .utf8))
        }
        task.resume()
    }

    func getHeroesByWord(_ word: String, completionHandler: @escaping ([Hero]) -> Void) {
        guard let url = URL(string: api + "people/?search=\(word)") else { return }
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: 15.0)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request) { (data, response, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            guard (response as? HTTPURLResponse)?.statusCode == 200,
                let data = data else { return }
            // BEFORE
            //            guard let jsonData = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else { return }
            //            print(jsonData["count"])
            
            
            // NOW
            guard let heroesResponse = try? JSONDecoder().decode(HeroesResponse.self, from: data) else { return }
            DispatchQueue.main.async {
                completionHandler(heroesResponse.results)
            }
            //            print(String(data: data, encoding: .utf8))
        }
        task.resume()
    }
}
