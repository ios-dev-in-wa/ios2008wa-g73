//
//  ViewController.swift
//  ios73ls14
//
//  Created by WA on 10/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var pageViewController: TutorialPageViewController?

    @IBOutlet weak var pageControll: UIPageControl!
    @IBOutlet weak var imageView: UIImageView!
    var timer: Timer?
   
    override func viewDidLoad() {
        super.viewDidLoad()
//        pageViewController?.set
        var counter = 1

        timer = Timer(fire: Date(), interval: 5, repeats: true, block: { _ in
            guard counter <= 3 else { return }
            self.pageControll.currentPage = counter
            counter += 1
        })
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) { [weak self] in
            guard counter <= 3 else { return }
            self?.pageControll.currentPage = counter
            counter += 1
        }
        
        guard let url = URL(string: "https://upload.wikimedia.org/wikipedia/commons/5/58/Sunset_2007-1.jpg") else { return }
        ImageDownloader.shared.loadImage(from: url) { [weak self] image in
            DispatchQueue.main.async {
                self?.imageView.image = image
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: segue)
        if let pageVC = segue.destination as? TutorialPageViewController {
            pageViewController = pageVC
            pageVC.tutorialDelegate = self
            pageControll.numberOfPages = pageVC.orderedViewControllers.count
//            pageVC.setViewControllers(viewControllers, direction: .forward, animated: true, completion: nil)
        }

    }

    @IBAction func didPressControl(_ sender: UIPageControl) {
        print(sender.currentPage)
    }
    //    @IBAction func didPressPageControll(_ sender: UIPageControl) {
//        pageViewController?.scrollToViewController(index: sender.currentPage)
//    }
}

extension ViewController: TutorialPageViewControllerDelegate {
    func tutorialPageViewController(tutorialPageViewController: TutorialPageViewController, didUpdatePageCount count: Int) {
        
    }
    
    func tutorialPageViewController(tutorialPageViewController: TutorialPageViewController, didUpdatePageIndex index: Int) {
        pageControll.currentPage = index
    }
}
