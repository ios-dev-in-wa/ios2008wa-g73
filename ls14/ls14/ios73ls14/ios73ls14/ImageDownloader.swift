//
//  ImageDownloader.swift
//  ios73ls14
//
//  Created by WA on 10/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ImageDownloader {
    
    static let shared = ImageDownloader()

    private let backgroundThread = DispatchQueue(label: "back", qos: .background, attributes: [], autoreleaseFrequency: .inherit, target: nil)

    let cache = NSCache<AnyObject, AnyObject>()

    func loadImage(from url: URL, completionHandler: @escaping ((UIImage?) -> Void)) {
        if let imageFromCache = cache.object(forKey: url as AnyObject) as? UIImage {
            completionHandler(imageFromCache)
            return
        }
        backgroundThread.async {
            URLSession.shared.dataTask(with: url) { [weak self] (data, response, error) in
                guard let data = data else { return }
                let image = UIImage(data: data)
                self?.cache.setObject(image as AnyObject, forKey: url as AnyObject)
                completionHandler(image)
            }.resume()
        }
    }
}
