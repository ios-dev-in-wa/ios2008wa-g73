//
//  BlueViewController.swift
//  ios73ls14
//
//  Created by WA on 10/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ImageViewController: UIPageViewController {

    @IBOutlet weak var imageView: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .purple
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard let url = URL(string: "https://upload.wikimedia.org/wikipedia/commons/5/58/Sunset_2007-1.jpg") else { return }
        ImageDownloader.shared.loadImage(from: url) { [weak self] image in
               DispatchQueue.main.async {
                   self?.imageView?.image = image
               }
           }
    }
}
