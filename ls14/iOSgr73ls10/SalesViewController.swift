//
//  SalesViewController.swift
//  iOSgr73ls10
//
//  Created by WA on 10/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

class SalesViewController: UIViewController {

    @IBOutlet weak var stackView: UIStackView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        let menuItemOne: MenuItemView = .fromNib()
        menuItemOne.onTapAction = {
            print("One")
        }
        let menuItemTwo: MenuItemView = .fromNib()
        menuItemTwo.onTapAction = {
            print("Two")
        }
        let menuItemThree: MenuItemView = .fromNib()
        menuItemThree.onTapAction = {
            print("Three")
        }
        
        [menuItemOne, menuItemTwo, menuItemThree].forEach {
            stackView.addArrangedSubview($0)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
