//
//  MenuItemView.swift
//  iOSgr73ls10
//
//  Created by WA on 10/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class MenuItemView: UIView {

    @IBOutlet weak var imageView: UIImageView!

    var onTapAction: (() -> Void)?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        onTapAction?()
    }
}
