//
//  NewViewController.swift
//  iOSgr73ls10
//
//  Created by admin on 9/30/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class NewViewController: UIViewController {

//    let myScrollView = UIScrollView()
//    let containerView = UIView()
//    let stackView = UIStackView()
//    let myButton1 = UIButton()
//    let myButton2 = UIButton()
//    let myButton3 = UIButton()
    @IBOutlet weak var firstButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        addButtons()
      
        firstButton?.imageView?.contentMode = .scaleToFill
    }
    
//    func addButtons() {
//        [myScrollView, containerView, stackView].forEach {
//            $0.translatesAutoresizingMaskIntoConstraints = false
//        }
//        view.addSubview(myScrollView)
//        NSLayoutConstraint.activate([
//            myScrollView.topAnchor.constraint(equalTo: view.topAnchor),
//            myScrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
//            myScrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
//            myScrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
//        ])
//
//        myScrollView.addSubview(containerView)
//        containerView.backgroundColor = .blue
//        let heightConstraint = containerView.heightAnchor.constraint(equalTo: myScrollView.heightAnchor)
//        heightConstraint.priority = .defaultLow
//        NSLayoutConstraint.activate([
//            containerView.topAnchor.constraint(equalTo: myScrollView.topAnchor),
//            containerView.bottomAnchor.constraint(equalTo: myScrollView.bottomAnchor),
//            containerView.leadingAnchor.constraint(equalTo: myScrollView.leadingAnchor),
//            containerView.trailingAnchor.constraint(equalTo: myScrollView.trailingAnchor),
//
//            containerView.widthAnchor.constraint(equalTo: view.widthAnchor),
//            heightConstraint
//        ])
//
//        let myImage1 = UIImage(named: "men.png")
//        let myImage2 = UIImage(named: "women.png")
//        let myImage3 = UIImage(named: "child.png")
//        var sizeY = view.frame.origin.y
//
////        myButton1 = UIButton(type: .custom)
//        myButton1.frame = CGRect(x: 0, y: sizeY, width: view.frame.width, height: myImage1!.size.height)
//        myButton1.setImage(myImage1, for: .normal)
//        myButton1.tag = 1
//        myButton1.addTarget(self, action: #selector(performMenVC), for: .touchUpInside)
//        sizeY += myImage1!.size.height + 5
//
////        myButton2 = UIButton(type: .custom)
//        myButton2.frame = CGRect(x: 0, y: sizeY, width: view.frame.width, height: myImage2!.size.height)
//        myButton2.setImage(myImage2, for: .normal)
//        myButton2.tag = 2
//        myButton2.addTarget(self, action: #selector(performMenVC), for: .touchUpInside)
//        sizeY += myImage2!.size.height + 5
//
////        myButton3 = UIButton(type: .custom)
//        myButton3.frame = CGRect(x: 0, y: sizeY, width: view.frame.width, height: myImage3!.size.height)
//        myButton3.setImage(myImage3, for: .normal)
//        myButton3.tag = 3
//        myButton3.addTarget(self, action: #selector(performMenVC), for: .touchUpInside)
//        sizeY += myImage3!.size.height
//
////        myScrollView = UIScrollView(frame: self.view.bounds)
//        myScrollView.addSubview(myButton1)
//        myScrollView.addSubview(myButton2)
//        myScrollView.addSubview(myButton3)
//        myScrollView.contentSize = CGSize(width: view.frame.width, height: sizeY)
//        self.view.addSubview(myScrollView)
//
//    }
    
    @objc func performMenVC(someButton: UIButton) {
        let menVC = MenViewController()
        let womenVC = WomenViewController()
        let childVC = ChildViewController()
        switch someButton.tag {
        case 1:
            navigationController?.pushViewController(menVC, animated: true)
        case 2:
            navigationController?.pushViewController(womenVC, animated: true)
        case 3:
            navigationController?.pushViewController(childVC, animated: true)
        default:
            return
        }
    }

}
