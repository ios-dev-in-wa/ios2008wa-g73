//
//  ViewController.swift
//  iosGr73ls2
//
//  Created by WA on 8/22/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
//        appAdminName
//        let someNumber = 1
//        var someString = "Artem"
//        print("1", someString)
////        someNumber = 22
//        someString = "Artem2"
//
//        print(someNumber)
//        print(someString)
//        makeGreeting(name: "Artem", count: 2)
//        makeGreeting(name: "Vadym", count: 5)

//        let someInt: Int = 1
//        let someDouble: Double = 2.0000222042324203402340
//        print(someInt, "Double", someDouble)
//        let result = someInt / Int(someDouble)
//        print(result)
//
//        let someOptional = someInt as? Double
//        print(someOptional)
//
//        print(Double(someInt))
//        squareOfNumber()
//        squareOf(number: 2)
//        squareOf(number: 6)
//        squareOf(number: 22)
//        let squareOfTwoConst = squareOfTwo()
//        print(squareOfTwoConst)
//        squareOfTwo()
        drawBox()
    }
    
    func makeGreeting(name: String, count: Int) {
        for _ in 0..<count {
            print("Hello, ", name)
        }
    }

    @discardableResult // Если результат не важен
    func squareOfTwo() -> Int {
        let firstNumber: Int = 2
        return squareOf(number: firstNumber)
    }

    func squareOf(number: Int) -> Int {
        let result = number * number
        print("The result of squering \(number), will be \(result)")
        return result
    }

    // UIView
    func drawBox() {
        let boxView = UIView()
        boxView.backgroundColor = UIColor.red
        boxView.frame.size.height = 40
        boxView.frame.size.width = 40
        boxView.frame.origin.x = 10
        boxView.frame.origin.y = 10
        view.addSubview(boxView)
    }
}

