//
//  ViewController.swift
//  ios71ls12
//
//  Created by WA on 6/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

struct MyStruct {
    var isOn = false
    var level = 2
}

class MyClass {
    var isOn = false
    var health = 100
}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        if Settings.shared.isScreenLocked {
//        }
        var myStructOne = MyStruct()
        let mySecondStruct = myStructOne
        myStructOne.level = 10
        print(mySecondStruct.level, myStructOne.level)

        let myClass = MyClass()
        let secondClass = myClass
        myClass.health = 0

        print(myClass.health, secondClass.health)

        CarSession().getCar(completion: procedeWithResult)
    }

    private func procedeWithResult(_ result: [Car]) {
        print(result)
    }
}

