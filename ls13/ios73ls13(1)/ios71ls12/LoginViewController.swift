//
//  LoginViewController.swift
//  ios71ls12
//
//  Created by WA on 6/26/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // screen bounds
//        print(UIScreen.main.nativeBounds)
        if UIDevice.current.userInterfaceIdiom == .pad {
            
        }
        [nameTextField, passwordTextField].forEach {
            $0?.delegate = self
        }
//        nameTextField.delegate = self
//        passwordTextField.delegate = self
//        UIDevice.current.orientation == .portrait
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
    }

    @IBAction func didPressLogin(_ sender: UIButton) {
        Settings.shared.isLoggedIn = true

        ViewManager.shared.setupInitialController()
    }

    @objc func keyboardWillAppear(notification: Notification) {
        let keyboardRect = notification.userInfo?["UIKeyboardBoundsUserInfoKey"] as? CGRect
//        print(keyboardRect?.height)
        let yPos = view.frame.height - stackView.frame.height - (keyboardRect?.height ?? 253)
        stackView.frame = CGRect(x: stackView.frame.origin.x, y: yPos, width: stackView.frame.width, height: stackView.frame.height)
    }
}

extension LoginViewController: UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField {
            passwordTextField.becomeFirstResponder()
        }
        if textField == passwordTextField {
            passwordTextField.resignFirstResponder()
        }
        return true
    }
}
