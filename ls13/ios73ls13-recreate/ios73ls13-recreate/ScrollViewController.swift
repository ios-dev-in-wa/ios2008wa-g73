//
//  ScrollViewController.swift
//  ios73ls13-recreate
//
//  Created by WA on 10/1/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class ScrollViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var nicknameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    private var activeTextField: UITextField?

    override func viewDidLoad() {
        super.viewDidLoad()
        [nameTextField, nicknameTextField, emailTextField, passwordTextField].forEach {
            $0?.delegate = self
        }
        setupObservers()
    }

    private func setupObservers() {
           NotificationCenter.default.addObserver(self, selector: #selector(willShowKeyboard), name: UIResponder.keyboardWillShowNotification, object: nil)
           NotificationCenter.default.addObserver(self, selector: #selector(willHideKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
       }

    @objc func willShowKeyboard(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            guard let textField = activeTextField else { return }
            let newConstant = view.safeAreaLayoutGuide.layoutFrame.height - textField.frame.maxY - keyboardHeight - textField.frame.height - 20
            scrollView.setContentOffset(CGPoint(x: 0, y: -newConstant), animated: true)
        }
    }

       @objc func willHideKeyboard() {
        scrollView.contentInset = .zero
       }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension ScrollViewController: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
            // set keyboard target to view
        case nameTextField: nicknameTextField.becomeFirstResponder()
        case nicknameTextField: emailTextField.becomeFirstResponder()
        case emailTextField: passwordTextField.becomeFirstResponder()
            // remove keyboard target from view
        default: textField.resignFirstResponder()
        }
        return true
    }
}
