//
//  Settings.swift
//  ios73ls5
//
//  Created by WA on 9/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

struct Settings {

    static var soundLevel: Int? {
        get { return UserDefaults.standard.value(forKey: "soundLevel") as? Int }
        set { UserDefaults.standard.set(newValue, forKey: "soundLevel")}
    }
}
