//
//  Example.swift
//  ios73ls5
//
//  Created by WA on 9/3/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Hero: NSObject {

    static let test = "Hero"

    override var description: String {
        return "Hero with health: \(health)"
    }
    let health: Int

    init(health: Int) {
        self.health = health
    }
}
