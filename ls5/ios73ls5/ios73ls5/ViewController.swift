//
//  ViewController.swift
//  iOSgr73ls4
//
//  Created by admin on 8/30/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

private struct Constant {
    static let viewHeight = 100
}

class ViewController: UIViewController {
    
    @IBOutlet weak var centralButton: UIButton!

    @IBOutlet weak var counterLabel: UILabel!

    var hero: Hero? = Hero(health: 100)
    var counter = 0

    weak var heroWeak: Hero?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
//        view.frame.size.height = CGFloat(Constant.viewHeight)
//        printDesctiptionExample()
//        print(Settings.soundLevel)
//        Settings.soundLevel = 100
//        print(Settings.soundLevel)
        
        centralButton.backgroundColor = .red
        centralButton.setTitle("KROKODIL", for: .normal)
        centralButton.setTitle("NotKROKODIL", for: .highlighted)
    }

    @IBAction func centralButtonDidPressed(_ sender: UIButton) {
        sender.backgroundColor = .random
        view.backgroundColor = .random
        counter += 1
        counterLabel.text = "TODAY HERE IS \(counter) KROKODILS"
    }
    //    func setExample() {
//        let some: Set<String> = ["Artem", "Inna", "Morty"]
//        let someTwo: Set<String> = ["Artem", "Valera", "Anatolyi"]
//        let test = some.intersection(someTwo)
//        print(test)
//    }
//
//    func checkIfTitleExist(_ title: String) { }
//
//    func setupWith(data: Data) { }
//
//    func printDesctiptionExample() {
//        let hero = Hero(health: 100)
//        print(hero)
//    }

//    func strongWeakExample() {
        // STRONG WEAK EXAMPLE
        //        print(hero)
        //        heroWeak = hero
        //        print(heroWeak)
        //        heroWeak = nil
        //        hero = nil
        //        print(heroWeak)
        //        print(hero)
//    }
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
