//
//  Hero.swift
//  ios73ls10
//
//  Created by WA on 9/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import Foundation

class Hero: Equatable {
    var healthPoints: Double = 0.0
    var atkDamage: Double = 0.0
    var speed: Double = 0.0

    init() { }
    
    init(health: Double, atkDamage: Double, speed: Double) {
        self.healthPoints = health
        self.atkDamage = atkDamage
        self.speed = speed
    }

    static func == (lhs: Hero, rhs: Hero) -> Bool {
        return lhs.healthPoints == rhs.healthPoints
    }
}

protocol Mage {
    func fireBall()
    func heal()

    var manaPool: Double { get set }
}

protocol Berserk {
    func rage()
    func killAllyToHeal()

    var rageTime: Double { get set }
}

class Hendalf: Hero, Mage {
    
    override init() {
        super.init()
        healthPoints = 100
        atkDamage = 50
        speed = 10
    }

    func fireBall() {
        print("YO YO YO FIREBALL")
    }
    
    func heal() {
        print("IM UNSTOPPABLE")
    }
    
    var manaPool: Double = 100.0
}

class Mordhau: Hero, Berserk {
    func rage() {
        print("RAGEEEE")
    }
    
    func killAllyToHeal() {
        print("OMNONOM")
    }
    
    var rageTime: Double = 10
}

let mord = Mordhau()
let hend = Hendalf()

func foo() {
    if mord is Mage {
        let equtable = mord == hend
    }
    if hend is Berserk {
        
    }
}
