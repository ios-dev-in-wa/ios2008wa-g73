//
//  ViewController.swift
//  ios73ls10
//
//  Created by WA on 9/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

typealias VoidCallback = (() -> Void)

class ViewController: UIViewController {
    
    @IBOutlet weak var faceView: FaceView!

    let heros: [Hero] = [
        Hero(health: 100, atkDamage: 0, speed: 100),
        Hero(health: 10, atkDamage: 150, speed: 1),
        Hero(health: 20, atkDamage: 200, speed: 10),
        Hero(health: 50, atkDamage: 10, speed: 15),
        Hero(health: 1000, atkDamage: 1, speed: 106)
    ]
    
    var optionalCallback: VoidCallback?

    let callback = {
        print("called")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let heroesWithDamage = heros.filter { hero -> Bool in
            hero.atkDamage > 0
        }
        let heroesWithDamage2 = heros.filter { $0.atkDamage > 0 }

        let sortedHeros = heros.sorted(by: { $0.atkDamage > $1.atkDamage })

        let firstWhere = heros.first(where: { $0.atkDamage == 10 })
        let lastIndex = heros.lastIndex(where: { $0.healthPoints == 100 })
        let health = heros.compactMap { String($0.healthPoints) }

        optionalCallback = foo

        optionalCallback?()
        
        faceView.changeHappyFace(withHappLevel: 10, andColor: .red)

        let gesture = UITapGestureRecognizer(target: self, action: #selector(tapAction))
        view.addGestureRecognizer(gesture)
        let swipeGesture = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeAction))
        swipeGesture.numberOfTouchesRequired = 2
        swipeGesture.direction = .left
        view.addGestureRecognizer(swipeGesture)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(pinchAction))

        view.addGestureRecognizer(pinchGesture)

        let rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(rotationAction))
        view.addGestureRecognizer(rotationGesture)
        
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(longPressAction))
        longPress.minimumPressDuration = 5
        longPress.allowableMovement = 40
        view.addGestureRecognizer(longPress)
    }

    @objc func longPressAction(sender: UILongPressGestureRecognizer) {
        switch sender.state {
        case .began:
            view.backgroundColor = .random
        case .cancelled:
            print("CANCELLED")
        case .changed:
            print("CHANGED")
        case .failed:
            print("FAILED")
        default: break
        }
    }

    @objc func tapAction() {
        let someView = UIView(frame: CGRect(x: 10, y: 10, width: 100, height: 100))
        someView.backgroundColor = .blue
//        UIView.transition(from: faceView, to: someView, duration: 0.5, options: [.transitionFlipFromTop, .autoreverse], completion: nil)
//        UIView.transition(with: faceView, duration: 0.5, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }

    @objc func leftSwipeAction(_ sender: UISwipeGestureRecognizer) {
        switch sender.state {
        case .ended:
            UIView.animate(withDuration: 0.5) {
                self.view.backgroundColor = .random
            }
        default: break
        }
    }

    @objc func pinchAction(sender: UIPinchGestureRecognizer) {
        print(sender.scale, sender.velocity)
//        sem
//        let someView = UIView(frame: CGRect(x: 10, y: 10, width: 100, height: 100))
//        someView.backgroundColor = .blue
        //        UIView.transition(from: faceView, to: someView, duration: 0.5, options: [.transitionFlipFromTop, .autoreverse], completion: nil)
        //        UIView.transition(with: faceView, duration: 0.5, options: [.transitionCrossDissolve], animations: nil, completion: nil)
    }

    @objc func rotationAction(sender: UIRotationGestureRecognizer) {

        if sender.state == .began || sender.state == .changed {
            faceView.transform = faceView.transform.rotated(by: sender.rotation)
            sender.rotation = 0
        }
    }

    func addCallback(optionalCallback: @escaping VoidCallback) {
        self.optionalCallback = optionalCallback
    }
    
    func foo() {
        print("CALLED")
    }
}


extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}
