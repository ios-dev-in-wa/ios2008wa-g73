//
//  CustomView.swift
//  ios73ls10
//
//  Created by WA on 9/19/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit

class CustomView: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .blue
    }

    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(rect: rect)
        path.lineWidth = 10
        path.move(to: CGPoint(x: 10, y: 10))
        path.addLine(to: CGPoint(x: 10, y: 10))
        path.addLine(to: CGPoint(x: 50, y: 50))
        path.addLine(to: CGPoint(x: 50, y: 10))
        path.addLine(to: CGPoint(x: 10, y: 10))
        path.close()

        let color = UIColor.red
        color.setStroke()
        UIColor.purple.setFill()
        path.fill()
        path.stroke()
    }
}
