class SwiftRobotControlCenter: RobotControlCenter {
    
    //  Level name setup
    override func viewDidLoad() {
        levelName = "L2H" //  Level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        //didSome()
        //moveUntilFrontIsClear()
        //turnLeft()
        for _ in 0...7 {
            moveAroundThePeak()
        }
        
    }
    
    func moveAroundThePeak() {
        moveToThePeak()
        moveToTheTopOfThePeak()
        moveToTheCorner()
    }
    
    func moveToThePeak() {
        while frontIsClear {
            move()
        }
    }
    
    func moveToTheTopOfThePeak() {
        while frontIsBlocked {
            turnRight()
            move()
            turnLeft()
        }
        
    }

    func turnLeft() {
        turnRight()
        turnRight()
        turnRight()
    }

    func moveToTheCorner() {
        move()
        turnLeft()
        moveToThePeak()
    }
}
