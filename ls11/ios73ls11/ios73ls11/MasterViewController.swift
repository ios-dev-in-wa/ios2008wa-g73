//
//  MasterViewController.swift
//  ios73ls11
//
//  Created by WA on 9/24/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import Parse

struct Car {
    let name: String
    let wasInAccident: Bool
    let productionYear: Int
    let id: String
}

class MasterViewController: UITableViewController {

    var detailViewController: DetailViewController? = nil
    var objects = [Car]()


    override func viewDidLoad() {
        super.viewDidLoad()
//        traitCollection.horizontalSizeClass
//        traitCollection.verticalSizeClass
        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = editButtonItem

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as? UINavigationController)?.topViewController as? DetailViewController
        }
        loadCars()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        super.viewWillAppear(animated)
    }
    
    func loadCars() {
        let query = PFQuery(className:"Car")
        
        query.findObjectsInBackground { [weak self] (objects, error) in
            if let cars = objects {
                self?.objects = cars.compactMap {
                    Car(name: $0["name"] as! String, wasInAccident: $0["wasInAccident"] as! Bool, productionYear: $0["productionYear"] as! Int, id: $0.objectId ?? "")
                }
                self?.tableView.reloadData()
            } else {
                print(error?.localizedDescription)
            }
        }
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "goToAddNewCarVC"?:
            guard let navC = segue.destination as? UINavigationController, let controller = navC.topViewController as? AddCarViewController else { return }
            controller.delegate = self
        default:
            break
        }
//        if segue.identifier == "showDetail" {
//            if let indexPath = tableView.indexPathForSelectedRow {
//                let object = objects[indexPath.row]
////                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
////                controller.detailItem = object
////                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
////                controller.navigationItem.leftItemsSupplementBackButton = true
//            }
//        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)

        let object = objects[indexPath.row]
        cell.textLabel?.text = object.name
        cell.detailTextLabel?.text = String(object.productionYear) + " year"
    
        return cell
    }

    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let query = PFQuery(className: "Car")
            query.getObjectInBackground(withId: objects[indexPath.row].id) { objects, error in
                objects?.deleteInBackground()
            }
            objects.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }

    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let action = UIContextualAction(style: .destructive, title: "Delete") { (action, view, boolHandler) in
            
        }
      return UISwipeActionsConfiguration(actions: [action])
    }

}

extension MasterViewController: AddCarViewControllerDelegate {
    func didAddNewCar() {
        loadCars()
    }
}
