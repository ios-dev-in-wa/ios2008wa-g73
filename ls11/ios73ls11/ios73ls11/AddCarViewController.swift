//
//  AddCarViewController.swift
//  ios73ls11
//
//  Created by WA on 9/24/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import Parse

protocol AddCarViewControllerDelegate: class {
    func didAddNewCar()
}

class AddCarViewController: UIViewController {

    @IBOutlet weak var carTextField: UITextField!
    @IBOutlet weak var wasInAccidentSwitch: UISwitch!
    @IBOutlet weak var productionyearTextField: UITextField!

    weak var delegate: AddCarViewControllerDelegate?

    @IBAction func saveAction(_ sender: UIBarButtonItem) {
        let object = PFObject(className: "Car")
        object["name"] = carTextField.text
        object["wasInAccident"] = wasInAccidentSwitch.isOn
        object["productionYear"] = Int(productionyearTextField.text ?? "0")

        object.saveInBackground { [weak self] (isSaved, error) in
            if isSaved {
                self?.delegate?.didAddNewCar()
                self?.cancelAction(sender)
            }
            print(error?.localizedDescription)
        }
    }
    
    @IBAction func cancelAction(_ sender: UIBarButtonItem) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
}
