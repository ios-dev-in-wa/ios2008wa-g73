//
//  DetailViewController.swift
//  ios73ls11
//
//  Created by WA on 9/24/19.
//  Copyright © 2019 WA. All rights reserved.
//

import UIKit
import AVFoundation

enum DataReceivingError: LocalizedError {
    case noData
    case notFound

    var localizedDescription: String {
        switch self {
        case .noData: return "No data is given for your request"
        case .notFound: return "404 not found"
        }
    }
}

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    var barButton: UIBarButtonItem?

    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            if let label = detailDescriptionLabel {
                label.text = detail.description
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureView()
        for _ in 0...5 {
            do {
                _ = try getSomeData()
            } catch {
                print((error as? DataReceivingError)?.localizedDescription)
            }
        }
        barButton = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(showPopover))
        navigationItem.rightBarButtonItem = barButton
        if UIDevice.current.userInterfaceIdiom == .pad {
            print("PAD")
        }
        if traitCollection.horizontalSizeClass == .compact {
           print("COMPACT")
        }
    }

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        if traitCollection.horizontalSizeClass == .compact {
            print("COMPACT")
        } else {
            print("REGULAR")
        }
    }

    @objc
    func showPopover() {
        guard let vc = storyboard?.instantiateViewController(withIdentifier: "popoerVC") else { return }
        vc.modalPresentationStyle = .popover
        if let popover = vc.popoverPresentationController {
            popover.barButtonItem = barButton
        }
        present(vc, animated: true, completion: nil)
    }

    var detailItem: NSDate? {
        didSet {
            // Update the view.
            configureView()
        }
    }

    @IBAction func playButtonACtion(_ sender: UIButton) {
        guard let url = Bundle.main.url(forResource: "alarm", withExtension: "mp3") else { return }
        do {
            let player = try AVAudioPlayer(contentsOf: url)
            player.play()
        } catch {
            print(error.localizedDescription)
        }
    }

    func getSomeData() throws -> [String] {
        if Int.random(in: 1...5) % 2 == 0 {
            return []
        } else {
            throw DataReceivingError.noData
        }
    }
}

